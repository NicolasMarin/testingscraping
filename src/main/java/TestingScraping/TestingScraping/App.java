package TestingScraping.TestingScraping;

import java.io.IOException;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class App {
	
	static String urlPage = "https://marvel.fandom.com/wiki/Amazing_Spider-Man_Vol_5";
	static String filter1 = "div.wikia-gallery-item";
	static String filter2 = "lightbox-caption";
	
	public static void main (String args[]) {
		
		System.out.println("Checking data from: " + urlPage);
		
		if (getStatusConnectionCode(urlPage) == 200) {// check if it gives me a 200 when making the request
			
			Document document = getHtmlDocument(urlPage);
			Elements entries = document.select(filter1);// filter the div with class wikia-gallery-item
			
			for (int i = 0; i < entries.size(); i ++) {
				System.out.println(getEntryInfo(entries.get(i)));
			}
		}else{
			System.out.println("The Status Code is not OK is: " + getStatusConnectionCode(urlPage));
		}
	}
	
	private static String getEntryInfo(Element element) {
    	String title = "", subtitle = "", date = "";
    	Elements lightBoxCaption = element.getElementsByClass(filter2);
    	Elements list = lightBoxCaption.get(0).children();
    	
    	String fullText = lightBoxCaption.get(0).text();
    	int index = getIndexStartDate(fullText);
    	title = list.get(0).children().get(0).text();
    	subtitle = fullText.substring(title.length() , index - 1 != title.length() ? index - 1 : index);
    	date = fullText.substring(index, fullText.length());
    	
    	return "Title: " + title + "   Subtitle: " + subtitle + "   Date: " + date;
	}

	private static int getIndexStartDate(String text) {
		int index = 0;
		for(int i = text.length() - 1; i > -1; i--) {
    		if(text.charAt(i) == '(') {
    			index = i;
    			break;
    		}
    	}
		return index;
	}

	/**
     * With this method I check the Status code of the response I receive when making the request
     * 		200 OK					300 Multiple Choices
     * 		301 Moved Permanently	305 Use Proxy
     * 		400 Bad Request			403 Forbidden
     * 		404 Not Found			500 Internal Server Error
     * 		502 Bad Gateway			503 Service Unavailable
     */
    public static int getStatusConnectionCode(String url) {
        Response response = null;
        try {
            response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true).execute();
        } catch (IOException ex) {
            System.out.println("Exception when obtaining the Status Code: " + ex.getMessage());
        }
        return response.statusCode();
    }
	
    /**
     * With this method get an object of the Document class with the HTML content 
     * of the web that will allow me to parse it with the methods of the JSoup library
     */
    public static Document getHtmlDocument(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).get();
        } catch (IOException ex) {
            System.out.println("Exception when obtaining the HTML of the page" + ex.getMessage());
        }
        return doc;
    }
}